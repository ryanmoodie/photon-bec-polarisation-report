\documentclass[9pt, a4paper, twocolumn]{extarticle}

\usepackage{graphicx, amsmath, hyperref, subcaption, cuted, sidecap}
\usepackage[hmargin=14mm, top=14mm, bottom=20mm]{geometry}
\usepackage[font=small]{caption}

\linespread{0.98}
\sidecaptionvpos{figure}{c}
\renewcommand{\abstractname}{}  

\title{
	\vspace{-4ex}
	\textbf{Polarisation in Photon Bose-Einstein Condensates}
	\vspace{-2ex}
	}
\author{
	Ryan Moodie\\
	Supervisor: Jonathan Keeling\\
	\textit{School of Physics and Astronomy, University of St Andrews}
	\vspace{-1.5ex}
	}
\date{
	August 2016\\
	\vspace{-7ex}
	}

\begin{document}

	\twocolumn[
		\begin{@twocolumnfalse}

			\maketitle

			\begin{abstract}
				Bose-Einstein condensation is an exotic state of matter in which a gas of bosons all occupy a single state, displaying quantum phenomena at macroscopic scales. Well observed with quasiparticles and cold atomic gases, the phase is generally impossible for the most common Bose gas: photons. Condensation is usually achieved at near-zero temperature; however, photons (assuming thermal equilibrium with the environment) are absorbed by surrounding matter on cooling, leaving nothing to condense. Nevertheless, a photon condensate was recently observed in a laser-pumped gas of photons in a dye-filled microcavity at room temperature \cite{klaers10}. Furthermore, currently unpublished experiments at Imperial College London following \cite{marelic14} indicate that the way light is polarised changes on condensation. While a photon condensation model exists \cite{kirton13}\cite{kirton15}\cite{keeling16}, there has been no previous work regarding its polarisation. Thus to describe this phenomenon, the theory was developed to include polarisation.
				\vspace{1ex}
			\end{abstract}

		\end{@twocolumnfalse}
	]

	\begin{figure}[!h]
		\begin{subfigure}[t]{0.49\linewidth}
			\centering
      \includegraphics[width=\textwidth]{diagram}
	    \caption{}
	    \label{fig:system}
		\end{subfigure}
		\begin{subfigure}[t]{0.49\linewidth}
		  \centering
	  	  \includegraphics[width=\textwidth]{report_population_distribution_0}
	      \caption{}
	      \label{fig:populationDistribution}
	    \end{subfigure}
	    \caption{(a) System diagram (from \cite{kirton15}). $\kappa$ refers to cavity photon loss, $\Gamma(\delta_{a})$ to molecular absorption of photons and $\Gamma(-\delta_{a})$ to emission, $\Gamma_{\uparrow}$ to pumping and $\Gamma_{\downarrow}$ to fluorescence, with these parameters being the rate of each. (b) Photon population distributions for weak (blue) pumping, showing a Bose-Einstein distribution, and strong (green) pumping, showing a condensation spectrum. Degenerate modes are summed, including polarised modes of equal $a$.}
  	\end{figure}

  \subsection*{Description of Experimental System}
		Two mirrors form the cavity (Figure \ref{fig:system}) such that photon mode energy levels are those of a two-dimensional harmonic oscillator. Photon population is maintained by laser-pumping the cavity. Dye molecules fill the cavity, considered as electronic two level systems dressed by rovibrational excitations. Photons can thermalise through molecular interaction, allowing an accumulation of low-energy photons which can lead to condensation for sufficiently low $\kappa$ and high $\Gamma_{\uparrow}$.

	\subsection*{Original Rate Equations}
		In the fully quantum model, a master equation describes the behaviour \cite{kirton13}. Through semi-classical approximation, more tractable rate equations can be obtained, describing the photon mode populations, $n_{a}$, and the molecular excited state population, $N_{\uparrow}$, without considering polarisation.

		Subscript $a$ refers to the $a^{th}$ photon mode, $g_{a}$ is a degeneracy factor equal to $a + 1$, and $N$ is the total number of molecules. 

		\begin{multline}
			\frac{\partial}{\partial t} \, n_{a} = - \kappa \, n_{a} \\ + \Gamma(-\delta_{a}) \, \Big( n_{a} + 1 \Big) \, N_{\uparrow} \,
			 - \Gamma(\delta_{a}) \, n_{a} \, \Big( N - N_{\uparrow} \Big)
			\label{eq:photonOriginal}
		\end{multline}

		\begin{multline}
			\frac{\partial}{\partial t} \, N_{\uparrow} = 
			- \, \Gamma_{\downarrow} \, N_{\uparrow} \, + \, \Gamma_{\uparrow} \Big( N - N_{\uparrow} \Big) \,
			+ \, \sum_{a=0}^{\infty} g_{a} \bigg \{ \\ \Gamma(\delta_a) \, n_a \, \Big( N - N_{\uparrow} \Big) \, - \, \Gamma(-\delta_a) \Big( n_{a} + 1 \Big) \, N_{\uparrow} \, \bigg \}
			\label{eq:moleculeOriginal}
		\end{multline}

	\vspace{1ex}
	\subsection*{Extension to include polarisation}
		To include polarisation, the equations were recast to include the angular dependence of the molecule-photon interaction. The photon modes were split into x- and y-polarised modes, an angular diffusion term (with coefficient $D$) was introduced to allow molecular rotation, and $N_{\uparrow}$ was generalised to $N_{\uparrow}(\theta, \phi)$. The integral over all angles of $N_{\uparrow}(\theta, \phi)$ is taken because the total emission into a given polarisation comes from a summation over all angles of $N_{\uparrow}(\theta, \phi)$ weighted by their couplings to that polarisation. 

		The solution was intractable in angular space, so $N_{\uparrow}(\theta, \phi)$ was expressed as a summation of spherical harmonics with coefficients $N_{l, m}$. By exploiting orthogonality of associated Legendre polynomials \cite{mathworld}, it was possible to obtain easily numerically solvable rate equations (\ref{eq:photonModified}) (\ref{eq:moleculeModified}).

		$\theta$ is the polar angle, $\phi$ the azimuthal angle and $\chi$ the pump polarisation angle. Superscript $\sigma$ refers to the polarisation state. 

		\begin{equation}
			\beta^{\sigma}(\varphi) = 
			\begin{cases}
				\cos^{2}(\varphi) & \text{for } \sigma = x \\
				\sin^{2}(\varphi) & \text{for } \sigma = y
			\end{cases}
			\label{eq:beta}
		\end{equation}

		\begin{strip}

			\begin{multline}
				\frac{\partial}{\partial t} \, n_{a}^{\sigma} = - \kappa \, n_{a}^{\sigma} 
				+ ( n_{a}^{\sigma} + 1 ) \, \Gamma(-\delta_{a}) \int_{0}^{\pi} d \theta \, \sin(\theta) \int_{0}^{2 \pi} d \phi \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, N_{\uparrow}(\theta, \phi) \\
				 - n_{a}^{\sigma} \, \Gamma(\delta_{a}) \int_{0}^{\pi} d \theta \, \sin(\theta) \int_{0}^{2 \pi} d \phi \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big)
				 \label{eq:photonAngle}
			\end{multline}

			\begin{multline}
				\frac{\partial}{\partial t} \, N_{\uparrow}(\theta, \phi) = \,
				- \, \Gamma_{\downarrow} \, N_{\uparrow}(\theta, \phi) \,
				+ \, \sum_{\sigma=(x, y)} \Bigg\{ \, \beta^{\sigma}(\chi) \, \Gamma_{\uparrow} \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big) \\ 
				+ \, \sum_{a=0}^{\infty} \, g_{a} \Bigg[ \, \Gamma(\delta_{a}) \, n_{a}^{\sigma} \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, \Big( N - N_{\uparrow}(\theta, \phi) \Big)
				- \Gamma(-\delta_{a}) \, ( n_{a}^{\sigma} + 1 ) \, \sin^{2}(\theta) \, \beta^{\sigma}(\phi) \, N_{\uparrow}(\theta, \phi) \Bigg] \Bigg\} \\
				+ \, D \, \Bigg[ \frac{1}{\sin^{2}(\theta)} \, \frac{\partial^{2}}{\partial \phi^{2}} + \frac{1}{\sin(\theta)} \, \frac{\partial}{\partial \theta} \bigg( \sin(\theta) \, \frac{\partial}{\partial \theta} \bigg) \Bigg] \, N_{\uparrow}(\theta, \phi)
				\label{eq:moleculeAngle}
			\end{multline}

		\end{strip}

		\begin{figure}[!htb]
				\centering
				\begin{subfigure}[t]{0.49\linewidth}
					\centering
		      \includegraphics[width=\textwidth]{report_pumping_response_photon_mode_0}
		      \caption{}
		      \label{fig:photonPopulations}
		    \end{subfigure}
				\begin{subfigure}[t]{0.49\linewidth}
		      \centering
		      \includegraphics[width=\textwidth]{report_pumping_response_excited_state_1}
			    \caption{}
		    	\label{fig:moleculePopulations}
				\end{subfigure}
				\caption{Populations against pumping rate with a fully x-polarised pump. (a) Only the first four photon modes are shown (higher energy modes follow the first mode populations but with successively lower occupation). (b) The molecular $N_{2, -2}$ population follows $N_{2, 2}$ exactly so is not explicitly shown and higher order coefficients are neglected as they are negligible.}
			\end{figure}

	\subsection*{Solving the Rate Equations}
		A program was written in Python using NumPy, SciPy \cite{scipy} and Matplotlib \cite{matplotlib} to solve the rate equations and investigate the steady state behaviour of the system. Parameters were set as in \cite{keeling16}. $D$ was set to match experimental data on the dye molecules \cite{bojarski96}. To test that the simulation was correct, photon population distributions were plotted (Figure \ref{fig:populationDistribution}) and found to match expectations.

    	Where $x$ and $y$ are the sums of all respectively polarised photon populations, the polarisation degree of the photon population, $P_{out}$, is defined as $\frac{x-y}{x+y}$. Correspondingly, pumping polarisation degree, $P_{in}$, is given by $\cos(2 \chi)$.

	\subsection*{System Behaviour}
		A clear threshold is seen in Figure \ref{fig:photonPopulations} at $\Gamma_{\uparrow} = 0.014$ GHz, at which $\Gamma_{\uparrow}$ is sufficiently high to maintain the photon population, allowing condensation. Throughout, lower energy and x-polarised modes are preferred. Below threshold, modes have similar occupation. Above threshold, the lowest two modes have occupation orders of magnitude higher than the others, showing condensation. Although the two condensed modes are degenerate, this multiple condensation mode behaviour is exotic.

		The molecular excited state occupation is related to $N_{0, 0}$. Higher order coefficients reflect the distribution of the molecular orientations (negative coefficients showing a deficit). In Figure \ref{fig:moleculePopulations}, higher order components are suppressed and $N_{0, 0}$ clamps above threshold.

		In such a situation of gain clamping, with both polarisation modes of the lowest energy condensed, the chemical potential reaches this energy level and the gain medium is in equilibrium. The excitation level of the molecules is then set by the energetics of the system, i.e. the thermal equilibrium, and so no angular dependence is expected. Thus it is expected that the higher order spherical harmonic components will go to zero as observed.

		Figure \ref{fig:2d_map} shows a low $P_{out}$ below threshold regardless of $P_{in}$. In this regime, emission is spontaneous and thus slow due to the small photon population. Therefore molecules rotate before emission, giving similarly sized polarisation populations. Far above threshold, photon populations are large so emission is stimulated and therefore fast. Molecules have little time to rotate before emission and so $P_{out}$ follows $P_{in}$. This effect weakens towards threshold until $P_{out}$ is almost fully polarised (except for an unpolarised pump). This reflects that one polarisation mode condenses first, giving an extremely polarised regime.		

		\begin{SCfigure}
			\includegraphics[width=0.75\linewidth]{report_2d_colour_map_0}
			\caption{
				Two dimensional colour map showing the response of $P_{out}$ to both $P_{in}$ and $\Gamma_{\uparrow}$. Threshold is again clear at $\Gamma_{\uparrow	} = 0.014$ GHz.
				}
			\label{fig:2d_map}
		\end{SCfigure}

	\subsection*{Future Outlook}
		On investigation of the response of $P_{out}$ to $D$, a curious result was found. For an x-polarised pump, above threshold, $P_{out}$ locks at unity as expected. However on removing any molecular angular diffusion (i.e. $D = 0$), $P_{out}$ remains small below threshold in contrast to $P_{in}$. In an attempt to understand this unexpected behaviour, the rate equations will be extended allowing for coherences between the polarisations.

		The response of $P_{out}$ to $P_{in}$ also suggested a potential hysteresis effect which will be explored.

	\subsection*{Student Experience}
		I have developed my skills in general problem solving and programming as well as in Physics-specific computational and analytical techniques. My self-leadership has improved in parallel with my maturing appreciation of leadership through working with my supervisor and the research group. This experience is directly applicable to postgraduate academia and, if the extension is successful, the project could result in a publication which would help secure such a future career.

	\subsection*{Conclusion}
		A computational simulation of the laser-pumped dye-filled microcavity system was created, allowing investigation of its polarisation behaviour. It predicts multiple mode condensation of both polarisation modes of the ground state. A weakly polarised photon gas is predicted below threshold and a strongly polarised photon condensate above threshold, with $P_{out}$ more closely following $P_{in}$ far above threshold. Some unusual behaviour has motivated a project extension.

	\bibliographystyle{plain}
	\bibliography{report}

	\begin{strip}

		\section*{Appendix}

			\begin{multline}
				\frac{\partial}{\partial t} \, n_{a}^{\sigma} = - \kappa \, n_{a}^{\sigma} + \frac{4 \pi}{3} \Bigg[ \Gamma(- \delta_a) \, \Big(n_{a}^{\sigma} + 1 \Big) \, \bigg \{ N_{0,0} - \frac{1}{\sqrt{5}} \, N_{2,0} + \alpha^{\sigma} \sqrt{\frac{3}{10}} \, \Big ( N_{2,2} + N_{2,-2} \Big ) \bigg \} \\ 
				- \Gamma(\delta_a) \, n_{a}^{\sigma} \, \bigg \{ N - N_{0,0} + \frac{1}{\sqrt{5}} \, N_{2,0} - \alpha^{\sigma} \sqrt{\frac{3}{10}} \, \Big ( N_{2,2} + N_{2,-2} \Big ) \bigg \} \Bigg] 
				\label{eq:photonModified}
			\end{multline}

			\begin{multline}
				\frac{\partial}{\partial t} \, N_{l,m} = - \, \Gamma_{\downarrow} \, N_{l,m} - D \, l \, \Big( l + 1 \Big) \, N_{l,m} 
				+ \sum_{\sigma=(x,y)} \, \Bigg \{ \, \beta^{\sigma}(\chi) \, \Gamma_{\uparrow} \, \mu_{l, m}^{\sigma} + \sum_{a=0}^{\infty} g_{a} \bigg[ \Gamma(\delta_a) \, n_a \, \mu_{l, m}^{\sigma} - \Gamma(-\delta_a) \Big( n_{a} + 1 \Big) \, \zeta_{l,m}^{\sigma} \bigg] \Bigg \}
				\label{eq:moleculeModified}
			\end{multline}

			\begin{equation}
				\alpha^{\sigma} = 
				\begin{cases}
					+ 1 & \text{for } \sigma = x \\
					- 1 & \text{for } \sigma = y
				\end{cases}
				\label{eq:alpha}
			\end{equation}

			\begin{equation}
				\mu_{l, m}^{\sigma} = \frac{N}{3} \Bigg ( \delta_{0, l} \delta_{0, m} - \frac{1}{\sqrt{5}} \, \delta_{2, l} \delta_{0, m} + \alpha^{\sigma} \sqrt{\frac{3}{10}} \, \bigg \{ \delta_{2,l} \delta_{2, m} + \delta_{2, l} \delta_{-2, m} \bigg \} \Bigg ) - \zeta_{l, m}^{\sigma}
				\label{eq:mu}
			\end{equation}

			\begin{multline}
				\zeta_{l, m}^{\sigma} = \frac{1}{2} \Bigg \{ \pi \, \alpha^{\sigma} \sqrt{\frac{(2 l + 1) \, (l - m)!}{4 \pi \, (l + m)!}} \, \sum_{l^{\prime} = 0}^{\infty} \Bigg [ \\ 
				\sqrt{\frac{(2 l^{\prime} + 1) \, (l^{\prime} - m + 2)!}{4 \pi \, (l^{\prime} + m - 2)!}} \int_{-1}^{1} dx \, \Big( 1-x^{2} \Big) \, P_{l, m}(x) \, P_{l^{\prime}, m - 2}(x) \ N_{l^{\prime}, m - 2} \\
				+ \, \sqrt{\frac{(2 l^{\prime} + 1) \, (l^{\prime} - m - 2)!}{4 \pi \, (l^{\prime} + m + 2)!}} \int_{-1}^{1} dx \, \Big( 1-x^{2} \Big) \, P_{l, m}(x) \, P_{l^{\prime},m + 2}(x) \ N_{l^{\prime}, m + 2} \, \Bigg ] \\ 
				+ \, \bigg ( 1 - \frac{(l - m + 1)(l + m + 1)}{(2 l + 1)(2 l + 3)} - \frac{(l + m)(l - m)}{(2 l + 1)(2 l - 1)} \bigg ) \, N_{l, m} \\
				- \, \frac{1}{2 l - 1} \sqrt{\frac{(l - m)(l - m - 1)(l + m)(l + m - 1)}{(2 l + 1)(2 l - 3)}} \, N_{l - 2, m} \\
				- \, \frac{1}{2 l + 3} \sqrt{\frac{(l - m + 2)(l - m + 1)(l + m + 2)(l + m + 1)}{(2 l + 1)(2 l + 5)}} \, N_{l + 2, m} \Bigg \} 
				\label{eq:zeta}
			\end{multline}

			\vspace{2ex}

			\centering

			The remaining integrals in $\zeta_{l, m}^{\sigma}$ were solved numerically.

	\end{strip}

	% Something is required after strip, so here's a placeholder:
	\hspace{1em}

\end{document}
